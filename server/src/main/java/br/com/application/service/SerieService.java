package br.com.application.service;

import br.com.application.model.School;
import br.com.application.model.Serie;
import br.com.application.repository.SchoolRepository;
import br.com.application.repository.SerieRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;

/**
 * Created by renatoromanini on 27/07/17.
 */
@Service
public class SerieService implements Serializable {

    private final Logger log = LoggerFactory.getLogger(SerieService.class);
    @Autowired
    private SerieRepository repository;

    @Transactional
    public Serie save(Serie obj) {
        log.debug("Request to save Serie : {}", obj);
        Serie result = repository.save(obj);
        return result;
    }

    @Transactional(readOnly = true)
    public List<Serie> findAll() {
        log.debug("Request to get all Serie");
        List<Serie> result = repository.findAll();
        return result;
    }

    @Transactional
    public void delete() {
        log.debug("Request to delete Serie");
        repository.deleteAll();
    }

}
