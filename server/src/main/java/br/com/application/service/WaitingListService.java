package br.com.application.service;

import br.com.application.model.WaitingList;
import br.com.application.repository.WaitingListRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;

/**
 * Created by renatoromanini on 27/07/17.
 */
@Service
public class WaitingListService implements Serializable {

    private final Logger log = LoggerFactory.getLogger(WaitingListService.class);
    @Autowired
    private WaitingListRepository repository;

    @Transactional
    public WaitingList save(WaitingList obj) {
        log.debug("Request to save WaitingList : {}", obj);
        WaitingList result = repository.save(obj);
        return result;
    }

    @Transactional(readOnly = true)
    public List<WaitingList> findAll() {
        log.debug("Request to get all WaitingList");
        List<WaitingList> result = repository.findAll();
        return result;
    }

    @Transactional
    public void delete() {
        log.debug("Request to delete WaitingList");
        repository.deleteAll();
    }

}
