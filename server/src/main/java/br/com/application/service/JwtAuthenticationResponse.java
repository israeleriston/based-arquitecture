package br.com.application.service;

import java.io.Serializable;

/**
 * Created by stephan on 20.03.16.
 */
public class JwtAuthenticationResponse implements Serializable {

    private static final long serialVersionUID = 1250166508152483573L;

    private final String token;
    private String user;

    public JwtAuthenticationResponse(String token, String user) {
        this.user = user;
        this.token = token;
    }

    public String getToken() {
        return this.token;
    }

    public String getUser() {
        return user;
    }
}
