package br.com.application.service;

import br.com.application.model.Student;
import br.com.application.model.Student;
import br.com.application.repository.StudentRepository;
import br.com.application.repository.StudentRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;

/**
 * Created by renatoromanini on 27/07/17.
 */
@Service
public class StudentService implements Serializable {

    private final Logger log = LoggerFactory.getLogger(StudentService.class);
    @Autowired
    private StudentRepository repository;

    @Transactional
    public Student save(Student obj) {
        log.debug("Request to save Student : {}", obj);
        Student result = repository.save(obj);
        return result;
    }

    @Transactional(readOnly = true)
    public List<Student> findAll() {
        log.debug("Request to get all Student");
        List<Student> result = repository.findAll();
        return result;
    }

    @Transactional
    public void delete() {
        log.debug("Request to delete Student");
        repository.deleteAll();
    }

}
