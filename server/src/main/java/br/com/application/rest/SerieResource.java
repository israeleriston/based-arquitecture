package br.com.application.rest;

import br.com.application.config.Constants;
import br.com.application.model.School;
import br.com.application.model.Serie;
import br.com.application.service.SchoolService;
import br.com.application.service.SerieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping(Constants.URL)
public class SerieResource {

    @Autowired
    private SerieService service;

    public SerieResource() {
    }

    @GetMapping("/serie")
    @CrossOrigin
    public List<Serie> getEntidades() {
        return service.findAll();
    }

    @PostMapping("/serie")
    public ResponseEntity<Serie> save(@Valid @RequestBody Serie entidade) throws URISyntaxException {
        Serie result = service.save(entidade);
        return ResponseEntity.status(HttpStatus.CREATED).body(result);
    }
}
