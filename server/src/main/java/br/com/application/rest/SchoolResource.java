package br.com.application.rest;

import br.com.application.config.Constants;
import br.com.application.model.School;
import br.com.application.service.SchoolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping(Constants.URL)
public class SchoolResource {

    @Autowired
    private SchoolService service;

    public SchoolResource() {
    }

    @GetMapping("/school")
    @CrossOrigin
    public List<School> getEntidades() {
        return service.findAll();
    }

    @PostMapping("/school")
    public ResponseEntity<School> save(@Valid @RequestBody School entidade) throws URISyntaxException {
        School result = service.save(entidade);
        return ResponseEntity.status(HttpStatus.CREATED).body(result);
    }
}
