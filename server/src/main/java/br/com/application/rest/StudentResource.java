package br.com.application.rest;

import br.com.application.config.Constants;
import br.com.application.model.Student;
import br.com.application.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping(Constants.URL)
public class StudentResource {

    @Autowired
    private StudentService service;

    public StudentResource() {
    }

    @GetMapping("/student")
    @CrossOrigin
    public List<Student> getEntidades() {
        return service.findAll();
    }

    @PostMapping("/student")
    public ResponseEntity<Student> save(@Valid @RequestBody Student entidade) throws URISyntaxException {
        Student result = service.save(entidade);
        return ResponseEntity.status(HttpStatus.CREATED).body(result);
    }
}
