package br.com.application.rest;

import br.com.application.config.Constants;
import br.com.application.model.School;
import br.com.application.model.Serie;
import br.com.application.model.enums.TypeSchool;
import br.com.application.service.SchoolService;
import br.com.application.service.SerieService;
import br.com.application.util.Util;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationHome;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.net.URISyntaxException;

/**
 * Created by renatoromanini on 29/07/17.
 */

@RestController
@RequestMapping(Constants.URL_PUBLICA)
public class LeitorExcelResource {

    @Autowired
    private SerieService serieService;
    @Autowired
    private SchoolService schoolService;


    @GetMapping("/leitor-excel")
    public ResponseEntity<String> comecarLeitura() throws URISyntaxException {
        String retorno = "veio aqui ";
        try {
            serieService.delete();
            schoolService.delete();
            ApplicationHome home = new ApplicationHome(this.getClass());
            File jarFile = home.getSource();
            File jarDir = home.getDir();
            String url = jarDir.getAbsolutePath() + "/server/src/main/resources/excel/002.xlsx";
            File file = new File(url);

            retorno += file.getName();

            org.apache.poi.ss.usermodel.Workbook workbook = WorkbookFactory.create(file);
            org.apache.poi.ss.usermodel.Sheet sheet = workbook.getSheetAt(0);
            int rowsCount = sheet.getLastRowNum();

            for (int i = 1; i <= rowsCount; i++) {

                Row row = sheet.getRow(i);
                if (row != null) {
                    int colCounts = row.getLastCellNum();
                    String nome = "";
                    String telefone = "";
                    String coordenado = "";
                    String endereco = "";
                    for (int j = 0; j < colCounts; j++) {
                        Cell cell = row.getCell(j);
                        String valorCell = Util.getValorCell(cell);


                        if (j == 0) {
                            nome = valorCell;
                        }
                        if (j == 1) {
                            telefone = valorCell;
                        }
                        if (j == 2) {
                            coordenado = valorCell;
                        }
                        if (j == 4) {
                            endereco = valorCell;

                            try {
                                endereco = endereco.replace("-", " ").replace(",", "");
                                RestTemplate restTemplate = new RestTemplate();
                                String result =
                                        restTemplate.getForObject("http://maps.googleapis.com/maps/api/geocode/json?address={" + endereco + "}",
                                                String.class);

                                JSONObject jsonObject = new JSONObject(result);
                                JSONArray results = (JSONArray) jsonObject.get("results");
                                JSONObject obj = (JSONObject) results.get(0);
                                JSONObject geometry = new JSONObject(obj.getString("geometry"));
                                JSONObject location = new JSONObject(geometry.getString("location"));

                                System.out.println(" lat " + location.getString("lat"));
                                System.out.println(" lng " + location.getString("lng"));
                            } catch (Exception e) {
                                System.out.println(endereco + " resultado  " + e.getMessage());
                            }


                        }
                    }
                    if (!nome.trim().isEmpty() && !coordenado.trim().isEmpty() && !endereco.trim().isEmpty()) {
                        saveSchool(nome, telefone, coordenado, endereco);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            retorno = " erroooor";
        }

        return ResponseEntity.status(HttpStatus.CREATED).body(retorno);
    }

    private void saveSchool(String nome, String telefone, String coordenado, String endereco) {
        School school = new School();
        school.setName(nome);
        school.setDirector(coordenado);
        school.setAndress(endereco);
        if (!telefone.trim().isEmpty()) {
            school.setPhone(telefone);
        }
        school.setTypeSchool(TypeSchool.ENSINO_MEDIO);
        school = schoolService.save(school);

        Serie serie = new Serie();
        serie.setName("1 ª Série");
        serie.setSchool(school);

        Serie save = serieService.save(serie);
    }

}
