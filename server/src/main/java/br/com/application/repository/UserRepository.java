package br.com.application.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import br.com.application.model.security.User;

/**
 * Created by stephan on 20.03.16.
 */
public interface UserRepository extends JpaRepository<User, Long> {
    User findByLogin(String login);
}
