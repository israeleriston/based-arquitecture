package br.com.application.repository;

import br.com.application.model.Student;
import br.com.application.model.WaitingList;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by renatoromanini on 27/07/17.
 */
public interface WaitingListRepository extends JpaRepository<WaitingList, Long> {

}
