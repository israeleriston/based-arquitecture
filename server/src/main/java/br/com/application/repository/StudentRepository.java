package br.com.application.repository;

import br.com.application.model.School;
import br.com.application.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by renatoromanini on 27/07/17.
 */
public interface StudentRepository extends JpaRepository<Student, Long> {

}
