import { http } from 'pluggables/http'

export const postLogin = (username, password) => {
  return http.post('/coletor/login', { username, password })
  .then(response => response.data)
}

export const getVerifyToken = (token) => {
  return http.post('/auth/verify', { token })
  .then(response => response.data)
}

export const getRefreshToken = (token) => {
  return http.post('/auth/refresh', { token })
  .then(response => response.data)
}
